<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--bootstrap and font awsome-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css"
        integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

    <!--AOS-->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />


    <title>Botstrap 4</title>
    <style>
       

      
    </style>
</head>

<body>
    <!-- Navbar Header -->
    <nav class="navbar navbar-light bg-light">
        <div class="container">
            <div class="navbar-brand mx-2">
                <img src="images/logo.svg" height="30" width="30" class="d-inline-block align-top" alt="">
                Vins_Shop
            </div>
        </div>
    </nav>

    <!--navbar-->
    <nav class="navbar  navbar-expand-lg navbar-dark sticky-top navbar-coba bg-dark  py-3">
        <div class="container">

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span> </button>

            <div class="collapse navbar-collapse " id="navbarResponsive">
                <ul class="navbar-nav mr-auto ">
                    <li class="nav-item mx-2 active">
                        <a class="nav-link active" aria-current="page" href="home.php">Home</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link mx-2 dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-expanded="false">
                            Product</a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="#">Website</a></li>
                            <li><a class="dropdown-item" href="#">Service Laptop</a></li>

                            <li><a class="dropdown-item" href="#">Jual Laptop</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link mx-2 dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-expanded="false">
                            Company
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="#">Action</a></li>
                            <li><a class="dropdown-item" href="#">Another action</a></li>

                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mx-2" aria-current="page" href="contact.php">Contact</a>
                    </li>

                </ul>

            </div>
        </div>
    </nav>
    <!--end-->

    <!-- carousel -->
    <div class="page-content my-3">
        <section class="coba-carousel">
            <div class="container">
                <div class="row ">
                    <div class="col-lg-12" data-aos="zoom-in">
                        <div id="cobaCarousel" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#cobaCarousel" data-slide-to="0" class="active"></li>
                                <li data-target="#cobaCarousel" data-slide-to="1"></li>
                                <li data-target="#cobaCarousel" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img src="images/banner5.jpg     " height="450px" class="d-block w-100"
                                        alt="Carousel Image" />
                                </div>
                                <div class="carousel-item">
                                    <img src="images/banner4.jpg" height="450px" class="d-block w-100"
                                        alt="Carousel Image" />
                                </div>
                                <div class="carousel-item">
                                    <img src="images/banner.jpg" height="450px" class="d-block w-100"
                                        alt="Carousel Image" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </section>
    <!--end-->

    <!-- content  -->
    <div class="container">


        <div class="row  py-5">
            <div class=" col-md-4" data-aos="fade-up" data-aos-delay="100">
                <div class="card bg-light">
                    <div class="card-body">






                        <h2>About</h2>
                        <p>Paragraph of text beneath the heading to explain the heading. We'll add onto it with another
                            sentence
                            and probably just keep going until we run out of words.</p>
                    </div>


                </div>
            </div>
            <div class=" col-md-4" data-aos="fade-up" data-aos-delay="200">

                <div class="card bg-light">
                    <div class="card-body">






                        <h2>Company</h2>
                        <p>Paragraph of text beneath the heading to explain the heading. We'll add onto it with another
                            sentence
                            and probably just keep going until we run out of words.</p>
                    </div>


                </div>


            </div>
            <div class=" col-md-4" data-aos="fade-up" data-aos-delay="300">

                <div class="card bg-light">
                    <div class="card-body">
                        <h2>Services</h2>
                        <p>Paragraph of text beneath the heading to explain the heading. We'll add onto it with another
                            sentence
                            and probably just keep going until we run out of words.</p>
                    </div>


                </div>

            </div>
        </div>
    </div>
    <!--end-->

    <!-- artikel -->

    <div class="container bg-light py-4">
        <div class="row">
            <div class="col-md-8 " data-aos="fade-left" data-aos-delay="400">
                <h2 class="pb-3 mb-3 ">
                    Content
                </h2>
                <p>Paragraph of text beneath the heading to explain the heading. We'll add onto it with another
                    sentence
                    and probably just keep going until we run out of words.</p>

                <h2>
                    Sub Header
                </h2>

                <p>Paragraph of text beneath the heading to explain the heading. We'll add onto it with another
                    sentence
                    and probably just keep going until we run out of words.</p>


            </div>

            <div class="col-md-4 " data-aos="fade-up" data-aos-delay="500">
                <h3 class="fst-italic">Navigation</h3>

                <div class="p-2">

                    <ul class="list-group">
                        <li class=" list-group-item d-flex justify-content-between align-items-center"><a
                                href="home.php">Home</a></li>
                        <li class=" list-group-item d-flex justify-content-between align-items-center"><a
                                href="#">Product</a> <span class="badge badge-primary badge-pill">6</span></li>
                        <li class="list-group-item d-flex justify-content-between align-items-center "><a
                                href="#">Company</a> <span class="badge badge-primary badge-pill">2</span></li>
                        <li class=" list-group-item d-flex justify-content-between align-items-center"><a
                                href="contact.php">Contact</a></li>

                        </ol>
                </div>

            </div>
        </div><!-- /.row -->
    </div><!-- /.container -->
    <!--end-->

    <!-- galery -->
    <div class="container py-5 ">
        <div class="row">
            <div class="col-md-12">
                <h2 class="pb-3 mb-3  text-center ">
                    Gallery
                </h2>
            </div>
        </div>
    
        <div class="container">
            <div class="row blog">
                <div class="col-md-12">
                    <div id="blogCarousel" class="carousel slide" data-ride="carousel">

                        <ol class="carousel-indicators">
                            <li data-target="#blogCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#blogCarousel" data-slide-to="1"></li>
                        </ol>

                        <!-- Carousel items -->
                        <div class="carousel-inner">

                            <div class="carousel-item active">
                                <div class="row">
                                    <div class="col-md-3"  data-aos="fade-up" data-aos-delay="100">
                                        
                                            <img src="images/macbook.png"  style="max-width:100%;">
                                        
                                    </div>
                                    <div class="col-md-3" data-aos="fade-up" data-aos-delay="200">
                                        
                                            <img src="images/dekstop.png" alt="Image" style="max-width:100%;">
                                        
                                    </div>
                                    <div class="col-md-3" data-aos="fade-up" data-aos-delay="300">
                                        
                                            <img src="images/111-coding.png" alt="Image" style="max-width:100%;">
                                        
                                    </div>
                                    <div class="col-md-3" data-aos="fade-up" data-aos-delay="400">
                                
                                            <img src="images/map.png" alt="Image" style="max-width:100%;">
                                        
                                    </div>
                                </div>
                                <!--.row-->
                            </div>
                            <!--.item-->

                            <div class="carousel-item">
                                <div class="row">
                                    <div class="col-md-3">
                                    
                                            <img src="images/app.png" alt="Image" style="max-width:100%;">
                                    </div>
                                    <div class="col-md-3">
                                      
                                            <img src="images/freelance.png" alt="Image" style="max-width:100%;">
                                    
                                    </div>
                                    <div class="col-md-3">
                                        
                                            <img src="images/gpu.png" alt="Image" style="max-width:100%;">
                                        
                                    </div>
                                    <div class="col-md-3">
                                        
                                            <img src="images/program.png" alt="Image" style="max-width:100%;">

                                    </div>
                                </div>
                                <!--.row-->
                            </div>
                            <!--.item-->

                        </div>
                        <!--.carousel-inner-->
                    </div>
                    <!--.Carousel-->

                </div>
            </div>
</div>
    </div>
 
    
    <!--end-->


    <footer class="footer bg-light p-3 ">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <p class="">
                    Copyright &copy; Daffa Hanaris 2021
                    </p>
                </div>
            </div>
        </div>
    </footer>


    <!-- JavaScript Bundle with Popper -->

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
    </script>

    <!--AOS-->
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>
</body>

</html>