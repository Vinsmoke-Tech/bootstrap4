<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">\
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">


    <title>Document</title>
    <style>
      .page-content {
            margin-top: 110px;
        }
        .nav-link {


margin-left: 10px;
margin-right: 10px;


}
    </style>
</head>
<body>
<!--navbar-->
<nav class="navbar  navbar-expand-lg navbar-dark navbar-coba bg-dark fixed-top navbar-fixed-top py-3">
        <div class="container">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"> <span
                    class="navbar-toggler-icon"></span> </button>

            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav me-auto ">
                    <li class="nav-item ">
                        <a class="nav-link " aria-current="page" href="home.php">Home</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-expanded="false">
                            Product</a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="#">Action</a></li>
                            <li><a class="dropdown-item" href="#">Another action</a></li>

                            <li><a class="dropdown-item" href="#">Something else here</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-expanded="false">
                            Company
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="#">Action</a></li>
                            <li><a class="dropdown-item" href="#">Another action</a></li>

                        </ul>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" aria-current="page" href="contact.php">Contact</a>
                    </li>

                </ul>

            </div>
        </div>
    </nav>

 <!--contact-->   
<div class="container page-content   ">
<h1 class="text-center">Contact Person</h1>
<hr>
  <div class="row">
    <div class="col-sm-8">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d247.19214214367423!2d110.77968257032877!3d-7.566881449444597!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a144bbb3eb13d%3A0x97a9fc1138e16c9e!2sKantor%20Kepala%20Desa%20Makamhaji!5e0!3m2!1sen!2sid!4v1617367936073!5m2!1sen!2sid"  width="100%" height="320" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>

    <div class="col-sm-4" id="contact2">
        <h3>Contact Personal</h3>
        <hr align="left" width="50%">
        <h4 class="pt-2">Daffa Ariftama Hanaris</h4>
        <i class="fas fa-globe" style="color:#000"></i> <a href="https://daffahanaris@blogspot.com"> Daffahanaris@blogspot.com<br></a>
        <h4 class="pt-2">Contact</h4>
        <i class="fas fa-phone" style="color:#000"></i> <a href=""> 089670674442 </a><br>
        <i class="fab fa-whatsapp" style="color:#000"></i><a href=""> 089670674442 </a><br>
        <h4 class="pt-2">Email</h4>
        <i class="fa fa-envelope" style="color:#000"></i> <a href="http://Daffahanaris@gmail.com"> Daffahanaris@gmail.com</a><br>
      </div>
  </div>
</div>





</div>
    <!-- JavaScript Bundle with Popper -->
 
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
    </script>
</body>
</html>